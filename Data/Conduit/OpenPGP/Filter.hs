-- Filter.hs: OpenPGP (RFC4880) packet filtering
-- Copyright © 2014-2018  Clint Adams
-- This software is released under the terms of the Expat license.
-- (See the LICENSE file).
{-# LANGUAGE GADTs #-}

module Data.Conduit.OpenPGP.Filter
  ( conduitPktFilter
  , conduitTKFilter
  , FilterPredicates(..)
  ) where

import Control.Monad.Trans.Reader (Reader, runReader)
import Data.Conduit (ConduitT)
import qualified Data.Conduit.List as CL

import Codec.Encryption.OpenPGP.Types

data FilterPredicates
  = RTKFilterPredicate (Reader TK Bool) -- ^ fp for transferable keys
  | RPFilterPredicate (Reader Pkt Bool) -- ^ fp for context-less packets

conduitPktFilter :: Monad m => FilterPredicates -> ConduitT Pkt Pkt m ()
conduitPktFilter = CL.filter . superPredicate

superPredicate :: FilterPredicates -> Pkt -> Bool
superPredicate (RPFilterPredicate e) p = runReader e p
superPredicate _ _ = False -- do not match incorrect type of packet

conduitTKFilter :: Monad m => FilterPredicates -> ConduitT TK TK m ()
conduitTKFilter = CL.filter . superTKPredicate

superTKPredicate :: FilterPredicates -> TK -> Bool
superTKPredicate (RTKFilterPredicate e) = runReader e
