-- Compression.hs: OpenPGP (RFC4880) compression conduits
-- Copyright © 2012-2018  Clint Adams
-- This software is released under the terms of the Expat license.
-- (See the LICENSE file).
module Data.Conduit.OpenPGP.Compression
  ( conduitCompress
  , conduitDecompress
  ) where

import Codec.Encryption.OpenPGP.Compression
import Codec.Encryption.OpenPGP.Types
import Control.Monad.Trans.Resource (MonadThrow)
import Data.Conduit
import qualified Data.Conduit.List as CL

conduitCompress :: MonadThrow m => CompressionAlgorithm -> ConduitT Pkt Pkt m ()
conduitCompress algo = CL.consume >>= \ps -> yield (compressPkts algo ps)

conduitDecompress :: MonadThrow m => ConduitT Pkt Pkt m ()
conduitDecompress = CL.concatMap decompressPkt
