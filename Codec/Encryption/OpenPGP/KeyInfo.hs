-- KeyInfo.hs: OpenPGP (RFC4880) fingerprinting methods
-- Copyright © 2012-2018  Clint Adams
-- This software is released under the terms of the Expat license.
-- (See the LICENSE file).
module Codec.Encryption.OpenPGP.KeyInfo
  ( pubkeySize
  , pkalgoAbbrev
  ) where

import qualified Crypto.PubKey.DSA as DSA
import qualified Crypto.PubKey.ECC.ECDSA as ECDSA
import qualified Crypto.PubKey.ECC.Types as ECCT
import qualified Crypto.PubKey.RSA as RSA
import Data.Bits (shiftR)
import Data.List (unfoldr)

import Codec.Encryption.OpenPGP.Types

pubkeySize :: PKey -> Either String Int
pubkeySize (RSAPubKey (RSA_PublicKey x)) = Right (RSA.public_size x * 8)
pubkeySize (DSAPubKey (DSA_PublicKey x)) =
  Right (bitcount . DSA.params_p . DSA.public_params $ x)
pubkeySize (ElGamalPubKey p _ _) = Right (bitcount p)
pubkeySize (ECDSAPubKey (ECDSA_PublicKey (ECDSA.PublicKey curve _))) =
  Right (fromIntegral (ECCT.curveSizeBits curve))
pubkeySize (ECDHPubKey (ECDSAPubKey (ECDSA_PublicKey (ECDSA.PublicKey curve _))) _ _) =
  Right (fromIntegral (ECCT.curveSizeBits curve))
pubkeySize (ECDHPubKey (EdDSAPubKey Ed25519 _) _ _) = Right 256
pubkeySize (EdDSAPubKey Ed25519 _) = Right 256
pubkeySize x = Left $ "Unable to calculate size of " ++ show x

bitcount :: Integer -> Int
bitcount =
  (* 8) .
  length .
  unfoldr
    (\x ->
       if x == 0
         then Nothing
         else Just (True, x `shiftR` 8))

-- FIXME: redo these for hOpenPGP 3
pkalgoAbbrev :: PubKeyAlgorithm -> String
pkalgoAbbrev RSA = "R"
pkalgoAbbrev DSA = "D"
pkalgoAbbrev ElgamalEncryptOnly = "g"
pkalgoAbbrev DeprecatedRSAEncryptOnly = "-"
pkalgoAbbrev DeprecatedRSASignOnly = "_"
pkalgoAbbrev ECDH = "e"
pkalgoAbbrev ECDSA = "E"
pkalgoAbbrev ForbiddenElgamal = "f"
pkalgoAbbrev DH = "d"
pkalgoAbbrev EdDSA = "w"
pkalgoAbbrev (OtherPKA _) = "."
